# Powiększony Nagłówek

To jest moje repozytorium z przykładowym plikiem README.md zawierającym składnię Markdown.

## Paragraf 1
Lorem ipsum dolor sit amet, consectetur adipiscing elit. *Phasellus* vitae justo et augue _fringilla_ dictum. **Pellentesque** habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

## Paragraf 2
Maecenas auctor euismod lacus, vel congue sapien facilisis vel. Integer aliquam, ~~urna~~ vel sodales pulvinar, purus libero euismod nunc, et vehicula elit nulla in odio.

## Paragraf 3
> "Wiedza jest potęgą." - Francis Bacon
![Logo](logo.png)

### Zagnieżdżona Lista Numeryczna
1. Pierwszy poziom
   1. Zagnieżdżony poziom
   2. Inny zagnieżdżony poziom
2. Drugi poziom
   1. Jeszcze jeden zagnieżdżony poziom
   2. I jeszcze jeden

### Zagnieżdżona Lista Nienumeryczna
- Element 1
  - Podpunkt A
  - Podpunkt B
- Element 2
  - Podpunkt C
  - Podpunkt D

### Blok Kodu
```python
def hello_world():
    print("Hello, world!")

hello_world()





